import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ElementRef, Renderer2 } from '@angular/core';

import Swal from 'sweetalert2';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { BotonesService } from '../servicios/botones.service';
import { MensajesService } from '../servicios/mensajes.service';
import { EnvioDataService } from '../servicios/envio-data.service';
import { CampaniaDataService } from '../servicios/campania-data.service';

@Component({
  selector: 'app-campania',
  templateUrl: './campania.component.html',
  styleUrls: ['./campania.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class CampaniaComponent implements OnInit {

   public mensajeSeleccionado:number = 0;
   public mostrarNoAsignados:boolean = true;

   constructor(
         private modalService : NgbModal,
         public activeModal : NgbActiveModal,
         public boton : BotonesService,
         public mensaje : MensajesService,
         public envioData : EnvioDataService,
         private renderer : Renderer2,
         public elementRef : ElementRef,
         private campaniaData : CampaniaDataService
      ) {
   }


  ngOnInit() {
 }

  resaltarMensaje(hover:boolean, order:number) {
    if(hover) {
      this.campaniaData.order = order
    }else {
      this.campaniaData.order = this.mensajeSeleccionado
    }
  }


  seleccionarMensaje(order) {
     if(this.campaniaData.mensajesData[order].activo) {
        this.mensajeSeleccionado = order
     }

     else{
        let hermanos = this.campaniaData.cargarMensajes(this.campaniaData.mensajesData[order].padre)

        if(hermanos.length > 0) {
           this.mensajeSeleccionado = hermanos[hermanos.length - 1].order;
        }

        else {
           this.mensajeSeleccionado = this.campaniaData.mensajesData[order].padre
        }
     }

     this.asignarEstiloAMsjSeleccionado()
  }


  private asignarEstiloAMsjSeleccionado() {
     let noSeleccionados = this.elementRef.nativeElement.querySelectorAll(`.objetoDeArbol`)
     let seleccionado = this.elementRef.nativeElement.querySelector(`#order${this.mensajeSeleccionado}`)

     noSeleccionados.forEach((elem) => {
        this.renderer.removeClass(elem, 'activo')
     })

     this.renderer.addClass(seleccionado, 'activo')
 }


  modal(modal) { this.activeModal = this.modalService.open(modal); }

  cerrarrModalMsj() { this.activeModal.close(); }

}
