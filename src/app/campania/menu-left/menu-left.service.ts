
import { Injectable, Input , Output, EventEmitter } from '@angular/core';
import { BehaviorSubject, Observable, of as observableOf} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class MenuLeftService {

    TreeDataMsj : any = {
    	title: "",
        subtitle : "",
        order: 0,
        childMsj: []
    }

    constructor() {}

    setTreeMessage(objMsj){
        this.TreeDataMsj =  objMsj
    }

    getTreeTitlesMsj() {
        return this.TreeDataMsj
    }
}
