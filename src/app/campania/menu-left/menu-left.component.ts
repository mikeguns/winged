import {Component, Injectable, OnInit, Input, Output, EventEmitter, ElementRef} from '@angular/core';
import { MenuLeftService } from './menu-left.service';
import { FormMsjService } from '../form-msj/form-msj.service';
import { MatDialog, MatDialogConfig} from "@angular/material";
import { DeleteMsjComponent } from '../delete-msj/delete-msj.component';

@Component({
    selector: 'menu-left',
    templateUrl: 'menu-left.component.html',
    styleUrls: ['menu-left.component.sass']
})
export class MenuLeftComponent implements OnInit {

    showListActions = false;
    firstTimeMsj : boolean;
    order:  number;
    orderMsjSelcted: number;
    
    treeListMsjFilled = {
        title: "Crecimiento",
        subtitle : "",
        order: 0,
        childMsj: [],
        listButtons: [],
        msjChild: false,
        orderFather: 0
    }
    shoMsjTitle: boolean;

    constructor(private _el:ElementRef, private dialog: MatDialog, private menuLeftService: MenuLeftService, private formMsjService: FormMsjService) {
        this.firstTimeMsj = true;
        this.order = 0;
        this.orderMsjSelcted = 0;
        this.shoMsjTitle = false;
    }

    ngOnInit(){

        this.formMsjService.getFilterMessageToMenu().subscribe((objMsj) => {
            if (objMsj.elementMsj.byButton) {
                this.firstTimeMsj = false;
            }
            if (objMsj.elementMsj.order == 0 && objMsj.elementMsj.orderFather == 0) {
                this.firstTimeMsj = true;
            }
            this.order = objMsj.elementMsj.order;
            this.childrenMsj(objMsj.elementMsj)
            this.menuLeftService.setTreeMessage(this.treeListMsjFilled)

        })

        this.formMsjService.getButtonChangeEmitter().subscribe((objButton) => {
            this.firstTimeMsj = false;
            this.treeListMsjFilled.childMsj[objButton.orderFather].listButtons.push(objButton)

        })

        this.formMsjService.getDeleteMsjButton().subscribe((msjButtonToDeleted)=> {
            this.deleteMsj(msjButtonToDeleted)
        })
    }

    childrenMsj(objMsj){

        let createChildMsj = (objMsjChild:any, childMsj:any) => {
            let childMsjValue = (childMsj.length == 0) ? [] : childMsj[0].childMsj;

            return {
                title: objMsjChild.title,
                subtitle: objMsjChild.subtitle,
                order: this.order,
                childMsj: childMsjValue,
                listButtons: [],
                msjChild: (objMsjChild.msjChild) ? objMsjChild.msjChild : false,
                orderFather: objMsjChild.orderFather
            }
        }

        let findObjMsj = (objMsjToFind) => {
            let childMsjs: any;

            if (objMsj.order === 0) {
                childMsjs = this.treeListMsjFilled.childMsj
                this.treeListMsjFilled.childMsj[objMsj.order] = createChildMsj(objMsj, childMsjs)
            } else {

                for (var i = 0; i < objMsjToFind.childMsj.length; i++) {

                    if (objMsjToFind.childMsj[i].order === objMsj.orderFather ) {

                        childMsjs = objMsjToFind.childMsj[i].childMsj;

                        objMsjToFind.childMsj[i].childMsj[objMsj.order -1] = createChildMsj(objMsj, childMsjs);
                        this.treeListMsjFilled = objMsjToFind;
                        break;
                    } else  {
                        findObjMsj(objMsjToFind)
                    }   
                }
            }
        }
        
        findObjMsj(this.treeListMsjFilled)
    }

    changePadding(orderChild){
        return {
            'padding-left': orderChild * 7 +'px'
        }
    }

    addMessage(msjSelected) {
        this.order++;
        this.formMsjService.setAddNewMessage(true, msjSelected)
        this.firstTimeMsj = false;
    }

    showMsj(objMsj) {
        this.orderMsjSelcted = objMsj.order;
        this.formMsjService.showMsjSelected(objMsj)
    }

    deleteMsj(objMsj){
        
        const dialogConfig = new MatDialogConfig();

        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;

        const dialogRef = this.dialog.open(DeleteMsjComponent, {
            height: '200px',
            width: '650px',
            data: objMsj
        });

        dialogRef.afterClosed().subscribe(objMsjtoDelete => {
            if (objMsjtoDelete.data != undefined) {

                let removeMsjFromList = (listMsj:any) => {

                    let filterMsj = (data) => {
                        return data.order != objMsjtoDelete.data.order
                    }

                    for (var i = 0; i < listMsj.childMsj.length; i++) {
                        if (listMsj.childMsj[i].order === objMsjtoDelete.data.order) {

                            if (listMsj.childMsj.length == 1) {
                                listMsj.childMsj = [];
                            } else {
                                for( var j = 0; j < listMsj.childMsj.length; j++){ 
                                    if ( listMsj.childMsj[j].order === objMsjtoDelete.data.order) {
                                        listMsj.childMsj.splice(j, 1);
                                        break; 
                                    }
                                }
                            }
                        } else {
                            removeMsjFromList(listMsj.childMsj[i])
                        }
                    }
                }

                removeMsjFromList(this.treeListMsjFilled)

                this.formMsjService.setDeletedMsj(true)
            }
        });

    }

}
