import { TestBed, inject } from '@angular/core/testing';

import { MenuLeftService } from './menu-left.service';

describe('MenuLeftService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MenuLeftService]
    });
  });

  it('should be created', inject([MenuLeftService], (service: MenuLeftService) => {
    expect(service).toBeTruthy();
  }));
});
