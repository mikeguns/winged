import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElegiMsjComponent } from './elegi-msj.component';

describe('ElegiMsjComponent', () => {
  let component: ElegiMsjComponent;
  let fixture: ComponentFixture<ElegiMsjComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElegiMsjComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElegiMsjComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
