import { TestBed, inject } from '@angular/core/testing';

import { ElegiMsjService } from './elegi-msj.service';

describe('ElegiMsjService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ElegiMsjService]
    });
  });

  it('should be created', inject([ElegiMsjService], (service: ElegiMsjService) => {
    expect(service).toBeTruthy();
  }));
});
