import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ElegiMsjService {

	msjSelected: any;

  	constructor() { }

  	setMsjSelected(objMsj){
  		this.msjSelected =  objMsj;
  	}

  	getMsjSelected(){
  		return this.msjSelected;
  	}


}
