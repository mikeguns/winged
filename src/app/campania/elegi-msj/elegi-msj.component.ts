import {Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig} from '@angular/material';
import {Message} from "./message";
import {MenuLeftService } from '../menu-left/menu-left.service';
import { FormButtonService} from '../form-button/form-button.service';
import { ElegiMsjService } from '../elegi-msj/elegi-msj.service';

@Component({
	selector: 'app-elegi-msj',
	templateUrl: './elegi-msj.component.html',
	styleUrls: ['./elegi-msj.component.sass']
})
export class ElegiMsjComponent implements OnInit {

	titleMessage: string;
	orderMessage: any;
	subtitleMessage: string;
	wrapDataButton: any;

	objMsjWritting: any;

	constructor(private menuLeftService: MenuLeftService, private formButtonService : FormButtonService,
		public dialogRef: MatDialogRef<ElegiMsjComponent>, 
		private dialog: MatDialog,
		private elegiMsjService: ElegiMsjService,
		@Inject(MAT_DIALOG_DATA) {order,title, subtitle}:Message ) {
			
			this.titleMessage = title;
			this.orderMessage = order;
			this.subtitleMessage =  subtitle

			this.wrapDataButton = {
				order: this.orderMessage,
				title: this.titleMessage,
				subtitle: this.subtitleMessage
			}

			this.objMsjWritting = this.elegiMsjService.getMsjSelected()
			console.log(this.objMsjWritting)
	}

	listMsj: any;
	arrayMsj: any = [{
		order: 0,
		title: ''
	}]

	ngOnInit() {
		this.listMsj = this.menuLeftService.getTreeTitlesMsj();
		this.arrayMsj = [];
		if (this.listMsj.childMsj.length > 0) {
				
			for (var i = 0; i < this.listMsj.childMsj.length; ++i) {
				if (this.objMsjWritting.order !== this.listMsj.childMsj[i].order) {
				
					this.arrayMsj.push({
						order: this.listMsj.childMsj[i].order,
						title: this.listMsj.childMsj[i].title,
						subtitle: this.listMsj.childMsj[i].subtitle
					})
				}
			}
		}
		
	}

	close(){
		this.dialogRef.close()
	}

	backToFormButton(objMessage){
		this.wrapDataButton = objMessage 
		this.dialogRef.close(this.wrapDataButton)
	}

}
