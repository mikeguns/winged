export interface Message {
	order: number,
	subtitle: string,
	title: string,
	orderFather: number,
	newOrderToMsj?: string
}
