import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteMsjComponent } from './delete-msj.component';

describe('DeleteMsjComponent', () => {
  let component: DeleteMsjComponent;
  let fixture: ComponentFixture<DeleteMsjComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteMsjComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteMsjComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
