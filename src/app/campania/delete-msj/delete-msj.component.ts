import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { MenuLeftComponent } from '../menu-left/menu-left.component';

export interface DialogMsj {
	tile: string;
	subtitle: string;
	order: number;
	childMsj: any;
}

@Component({
	selector: 'app-delete-msj',
	templateUrl: './delete-msj.component.html',
	styleUrls: ['./delete-msj.component.css']
})
export class DeleteMsjComponent implements OnInit {

	statusDeleteMsj : any

	constructor( public dialogRef: MatDialogRef<MenuLeftComponent>,
		@Inject(MAT_DIALOG_DATA) public data: DialogMsj) { 
		this.statusDeleteMsj = {
			confirm: false,
			data: {}
		}
	}

	ngOnInit() {}

	confirmDelete(){
		this.statusDeleteMsj.confirm = true;
		this.statusDeleteMsj.data =  this.data
		this.dialogRef.close(this.statusDeleteMsj);
	}

}
