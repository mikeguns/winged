import { Injectable, Input , Output, EventEmitter } from '@angular/core';
import { MsjObject ,BodyStep ,StepContent ,Attachment ,Payload ,Element ,Button } from './form-msj';
import { MenuLeftService } from '../menu-left/menu-left.service';


@Injectable({
	providedIn: 'root'
})
export class FormMsjService {

	buttonMsjList 	  : EventEmitter<any> = new EventEmitter();
	countMsj 	  	  : number = 0;
	filterMessage 	  : EventEmitter<any> = new EventEmitter();
	newMsjToWrite 	  : EventEmitter<any> = new EventEmitter();
	msjSelected	      : EventEmitter<any> = new EventEmitter();
	listTotalMsj      : EventEmitter<any> = new EventEmitter();
	deleteMsjButton   : EventEmitter<any> = new EventEmitter();
	msjOrderInit      : EventEmitter<number> = new EventEmitter();
	resetForm         : EventEmitter<boolean> = new EventEmitter(); 

	msjObject     : MsjObject;
	msjBodyStep   : Array<BodyStep>;
	msjStepContent: Array<StepContent>;
	msjAttachment : Attachment;
	msjPayload    : Payload;
	msjElements   : Array<Element>;
	msjButtons    : Array<Button> = [];
	msjOrder      : number;
	itemStep      : any;
	itemStepChild : any;	

	statusMessageSelected: boolean;

	constructor(private menuLeftService: MenuLeftService) { 
		let itemStepObj =  this.buildiIemStep();
		this.msjBodyStep = [itemStepObj];
		this.statusMessageSelected = false
	}

	buildiIemStep(){
		this.msjOrder = 0;

		this.msjElements = [{
			title: '',
			subtitle: '',
			buttons: []
		}]

		this.msjPayload = {
			template_type : 'generic',
			elements: this.msjElements 
		}

		this.msjAttachment = {
			type : 'template',
			payload: this.msjPayload
		}	

		this.msjStepContent = [{
				attachment: this.msjAttachment
		}]

		this.itemStep = {
			stepContent: this.msjStepContent,
			order : this.msjOrder
		}

		return this.itemStep
	}

	emitButtonsChangeEvent(buttons) {	
		this.msjButtons.push({
			title : buttons.title,
			type: 'postback',
			payload: '#type#step#type##order#'+buttons.order+'#order',
			orderFather: buttons.orderFather
		});
		this.buttonMsjList.emit(buttons);
	}

	getButtonChangeEmitter() {
		return this.buttonMsjList;
	}

	getListButtons(){
		return this.msjButtons
	}

	buildTreeMsj() {
		this.msjObject = {
			startingStep : { stepContent: this.msjStepContent, order : this.msjOrder } ,
			bodySteps:  [] 
		}
		return this.msjObject;
	}

	createNewMsjObject() {
		let itemStepObjChild =  this.buildiIemStep()
		this.msjObject.bodySteps.push(itemStepObjChild)
		return this.msjObject;
	}

	setFilterMessageToMenu(elementMsj:any){
		let filterMsj = {};
		filterMsj = { 'elementMsj' : elementMsj }
		this.filterMessage.emit(filterMsj)
	}

	getFilterMessageToMenu(){
		return this.filterMessage
	}

	setAddNewMessage(newMsj:boolean, msjSelected:any){
		this.newMsjToWrite.emit({
			addNewMsj: newMsj,
			msjChild: msjSelected})
	}

	getAddNewMessage(){
		return this.newMsjToWrite;
	}

	showMsjSelected(objMsjSelected:any) {
		this.setStatusMessageSelected(true)
		this.msjSelected.emit(objMsjSelected);
	}

	getMsjSelected(){
		return this.msjSelected;
	}

	setStatusMessageSelected(type:boolean){
		this.statusMessageSelected = type
	}

	getStatusMessageSelected(){
		return this.statusMessageSelected
	}

	setDeleteMsjButton(objMsjToDeleted:any){
		this.deleteMsjButton.emit(objMsjToDeleted);
	}

	getDeleteMsjButton(){
		return this.deleteMsjButton;
	}

	setDeletedMsj(statusReset:boolean){
		this.resetForm.emit(statusReset)
	}

	getDeletedMsj(){
		return this.resetForm
	}
	
}
