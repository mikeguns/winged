export interface MsjObject {
	pageAccessToken?: string;
	pageId?: string;
	type?: string;
	startingStep: BodyStep;
	bodySteps?: BodyStep[];
}


export interface BodyStep {
	stepContent?: StepContent[];
	order?: number;
}

export interface StepContent {
	attachment: Attachment;
}

export interface Attachment {
	type?: string ;
	payload?: Payload;
}

export interface Payload {
	template_type: string;
	elements: Element[];
}

export interface Element {
	title: string;
	image_url?: string;
	subtitle?: string;
	buttons: Button[];
}

export interface Button {
	type: string;
	url?: string;
	title: string;
	payload: string;
	orderFather: number;
}
