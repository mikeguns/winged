import { TestBed, inject } from '@angular/core/testing';

import { FormMsjService } from './form-msj.service';

describe('FormMsjService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FormMsjService]
    });
  });

  it('should be created', inject([FormMsjService], (service: FormMsjService) => {
    expect(service).toBeTruthy();
  }));
});
