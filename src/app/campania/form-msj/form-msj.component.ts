import { Component, OnInit, Input, ViewChild, AfterViewInit,  Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogConfig} from "@angular/material";
import { FormButtonComponent } from '../form-button/form-button.component';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MenuLeftService } from "../menu-left/menu-left.service";
import { FormMsjService} from "./form-msj.service";
import { ElegiMsjService } from "../elegi-msj/elegi-msj.service";
import { FormButtonService } from "../form-button/form-button.service";
import { MenuLeftComponent } from "../menu-left/menu-left.component";
// setMsjSelected

export interface IMsjWritting {
    title: string,
    subtitle: string,
    order: number,
    orderFather?: number,
    msjChild?: boolean
}    


export interface IButton {
    type: string,
    payload: string,
    title: string,
    orderBtn: number,
    orderFather: number
}

export interface IMsjCreated {
    orderFather: number
}

@Component({
    selector: 'app-form-msj',
    templateUrl: './form-msj.component.html',
    styleUrls: ['./form-msj.component.sass']
})
export class FormMsjComponent implements OnInit {

    msjForm: FormGroup;
    listButtons: IButton[] = [];
    enableAddButon: boolean;
    indexChildMsj : number;
    order: number;
    mainStructureMsj: any = {};
    statusResetForm: boolean;
    statusMessageSelected: boolean;

    msjFormWritting: IMsjWritting;

    msjCreatedByButton: IMsjCreated;
    getByButtonMsj: boolean = false;

    newOrderMsjCreated: number;

    constructor(public formButtonService: FormButtonService, private elegiMsjService: ElegiMsjService,  private dialog: MatDialog, private menuLeftService: MenuLeftService, private formMsjService: FormMsjService) {
        this.enableAddButon = false; 
        this.order = 0;
        this.statusResetForm = false;
        this.listButtons = [];
        this.newOrderMsjCreated = 0;
    }

    ngOnInit() {
        this.msjCreatedByButton = {
            orderFather: 0
        }

        this.msjFormWritting = {
            title: '',
            subtitle: '',
            order: 0,
            orderFather: 0,
            msjChild: false
        }

        this.indexChildMsj = -1;
        this.msjForm = new FormGroup({
            title: new FormControl('',Validators.required),
            subtitle: new FormControl('',Validators.required),
        });

        this.statusMessageSelected = this.formMsjService.getStatusMessageSelected()

        this.mainStructureMsj = this.formMsjService.buildTreeMsj()

        this.formButtonService.getNewMsjButton().subscribe((objFormButton) => {
            this.listButtons = [];      
            this.formatFormToButtons(objFormButton)
        })

        this.formMsjService.getDeletedMsj().subscribe(status => this.resetMsj())

        this.formMsjService.getMsjSelected().subscribe((valMsj) => {

            this.listButtons = valMsj.listButtons;
            let msjToShowinForm: any;
            this.getByButtonMsj = false;
            
            this.msjForm.patchValue({
                title : valMsj.title,
                subtitle : valMsj.subtitle
            })
            if (this.listButtons.length > 0) {
                this.msjCreatedByButton.orderFather = valMsj.orderFather;
            }
            this.order = valMsj.order

            this.msjFormWritting = this.fillFormWritting(valMsj)

            this.elegiMsjService.setMsjSelected(this.msjFormWritting)
        })

        this.formMsjService.getAddNewMessage().subscribe((createNewMsj) => {
            if (createNewMsj.addNewMsj) {

                this.msjCreatedByButton.orderFather = createNewMsj.msjChild.orderFather;

                if (createNewMsj.msjChild.childMsj.length > 0) {
                    this.order =  createNewMsj.msjChild.childMsj[createNewMsj.msjChild.childMsj.length -1].order +1;
                } else {
                    this.order =  1;
                }

                this.msjForm.reset(); 
            }
        })

        this.onChanges(this.statusMessageSelected)

    }

    openFormButton() {
        let listMessagesMenuLeft = [];
        const dialogConfig = new MatDialogConfig();

        let getLengthMsjChildren = (listMsj:any) => {
            console.log(listMsj)

                if (listMsj.childMsj.length > 0) {
                    for (var i = 0; i < listMsj.childMsj.length; i++) {
                        if (listMsj.childMsj[i].order === this.msjFormWritting.order) {
                            this.newOrderMsjCreated = listMsj.childMsj[i].childMsj.length +1;
                            break;
                        } else {
                            getLengthMsjChildren(listMsj.childMsj[i].childMsj)
                        }
                    }
                }
        }

        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;

        listMessagesMenuLeft =  this.menuLeftService.getTreeTitlesMsj()

        getLengthMsjChildren(listMessagesMenuLeft)

        this.msjFormWritting['newOrderToMsj'] = this.newOrderMsjCreated;

        const dialoOpen =  this.dialog.open(FormButtonComponent, {
            height: '330px',
            width: '330px',
            data:  this.msjFormWritting
        });
        if (this.listButtons.length === 2) {
            this.enableAddButon = true;
        }

    }

    onChanges(status): void {

        this.msjForm.valueChanges.subscribe(msjWritting => {
            let statusSelected = this.formMsjService.getStatusMessageSelected()
            if (!statusSelected) {
                if (msjWritting.title != null) {
                    if (!this.statusResetForm) {
                        this.mainStructureMsj.startingStep.order =  this.order
                        this.mainStructureMsj.startingStep.stepContent[0].attachment.payload.elements[0].title =  msjWritting.title;        
                        this.mainStructureMsj.startingStep.stepContent[0].attachment.payload.elements[0].subtitle =  msjWritting.subtitle;        
                        this.mainStructureMsj.startingStep.stepContent[0].attachment.payload.elements[0].buttons = this.listButtons
                    } else {
                        this.mainStructureMsj.bodySteps[this.indexChildMsj].order = this.order
                        this.mainStructureMsj.bodySteps[this.indexChildMsj].stepContent[0].attachment.payload.elements[0].title =  msjWritting.title;        
                        this.mainStructureMsj.bodySteps[this.indexChildMsj].stepContent[0].attachment.payload.elements[0].subtitle =  msjWritting.subtitle;
                        this.mainStructureMsj.bodySteps[this.indexChildMsj].stepContent[0].attachment.payload.elements[0].buttons =  this.listButtons;

                        console.log('mainObject ' + JSON.stringify(this.mainStructureMsj))
                    }
                    
                    this.msjFormWritting = this.fillFormWritting(msjWritting)

                    this.elegiMsjService.setMsjSelected(this.msjFormWritting)

                    this.formMsjService.setFilterMessageToMenu(this.msjFormWritting)
                }
            } else {
                this.formMsjService.setStatusMessageSelected(false) 
            }
        })
    }

    fillFormWritting(msjWritting){
        return {
            title: msjWritting.title,
            subtitle: msjWritting.subtitle,
            order: this.order,
            orderFather:  this.msjCreatedByButton.orderFather,
            byButton: this.getByButtonMsj
        }
    }

    formatFormToButtons(objFormButton) {
        console.log(objFormButton)
        this.order = objFormButton.msjFather.newOrderToMsj;
        this.getByButtonMsj = true;

        this.msjForm.patchValue({
            title : 'Paso'+this.order,
            subtitle : '',
        })

        let objButtons = {
            title: objFormButton.msjFather.title,
            order: objFormButton.msjFather.order,
            orderFather: objFormButton.msjFather.orderFather
        }

        this.formMsjService.emitButtonsChangeEvent(objButtons)
    }

    newMsjAdded(){
        this.msjForm.reset();
        this.order++;
        this.indexChildMsj++;
    }

    deleteMsjComplete() {
        this.msjForm.reset();
        this.order = 0;
        this.indexChildMsj = 0;
        this.listButtons = [];
        this.mainStructureMsj = this.formMsjService.createNewMsjObject()
    }

    resetMsj(){
        this.msjForm.reset();
        this.listButtons = [];
    }

    deleteMsg(nameBtn:string) {
        this.listButtons = this.listButtons.filter(item => item.title !== nameBtn);        
    }

    cancelBtnMsj(){
        this.formMsjService.setDeleteMsjButton(this.msjFormWritting)
    }
}
