import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormMsjComponent } from './form-msj.component';

describe('FormMsjComponent', () => {
  let component: FormMsjComponent;
  let fixture: ComponentFixture<FormMsjComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormMsjComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormMsjComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
