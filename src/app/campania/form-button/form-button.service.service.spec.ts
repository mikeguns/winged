import { TestBed, inject } from '@angular/core/testing';

import { FormButton.ServiceService } from './form-button.service.service';

describe('FormButton.ServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FormButton.ServiceService]
    });
  });

  it('should be created', inject([FormButton.ServiceService], (service: FormButton.ServiceService) => {
    expect(service).toBeTruthy();
  }));
});
