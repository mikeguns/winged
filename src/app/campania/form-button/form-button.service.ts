import { Injectable, Input , Output, EventEmitter } from '@angular/core';
import { MenuLeftService } from '../menu-left/menu-left.service';

export interface ButtonTemporal {
    status: boolean,
    title: string
}

export interface IListButtonTemporal {
    data: any,
    msjFather: any,
    idButton: number
}

@Injectable({
    providedIn: 'root'
})
export class FormButtonService {

	setNewMessageBtn: EventEmitter<any> = new EventEmitter();
	msjByRole  : EventEmitter<any> = new EventEmitter();
    listButtonsCreated: Array<IListButtonTemporal>
    constructor(private menuLeftService: MenuLeftService) { 
        this.listButtonsCreated = [];
    }

    findMsjByRole(orderMsj) {
        this.msjByRole.emit(this.findMsj(orderMsj))
    }

    findMsj(order){
        let getMessage = this.menuLeftService.getTreeTitlesMsj()
        return getMessage.childMsj.filter(item => item.order == order )
    }

    getMessageByRole(){
        return this.msjByRole
    }

    setNewMsjButton(objButton:any, msjFather:any){
            
        let listButtons = this.getListButtonsCreated(); 
      
        let LastButton = (listButtons.length > 0 ) ? listButtons[listButtons.length - 1].idButton + 1 :  1;

        let dataMsjbutton = {
            data: objButton.formWritting,
            msjFather: msjFather,
            idButton: LastButton
        }
        this.listButtonsCreated.push(dataMsjbutton)
        this.setNewMessageBtn.emit(dataMsjbutton)
    }

    getNewMsjButton(){
        return this.setNewMessageBtn
    }

    getListButtonsCreated(){
        return this.listButtonsCreated
    }
    
    
}
