import { Component, Inject, OnInit, Input} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig} from '@angular/material';
import { ElegiMsjComponent } from '../elegi-msj/elegi-msj.component';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FormMsjService } from '../form-msj/form-msj.service'; 
import { FormButtonService } from './form-button.service';
import { Message } from '../elegi-msj/message';
import { ElegiMsjService } from '../elegi-msj/elegi-msj.service';
import { FormMsjComponent } from '../form-msj/form-msj.component';


@Component({
	selector: 'app-form-button',
	templateUrl: './form-button.component.html',
	styleUrls: ['./form-button.component.sass']
})
export class FormButtonComponent implements OnInit {

	@Input() message: Message[];

	titleModal: string;
	buttonForm: FormGroup;
	enabledMsj: boolean = false;
	newMsj: string;
	msjFoundByRole: string;
	titleForm: string;
	msjFather: any;
	idButton: number;
	listButtonsCreated: any;
	orderMsjDefault: number;

	constructor(public dialogRef: MatDialogRef<FormButtonComponent>,
		public dialogFormMsj: MatDialogRef<FormMsjComponent> ,
		@Inject(MAT_DIALOG_DATA) public data: Message,
		private dialog: MatDialog,
		private formMsjService: FormMsjService,
		private formButtonService: FormButtonService,
		private elegiMsjService: ElegiMsjService ,
		) {
		this.newMsj = "Nuevo Mensaje";
		this.buttonForm = new FormGroup({
			title: new FormControl('',Validators.required) 
		});
		this.titleForm = 'Comportamiento al presionar';
		this.listButtonsCreated = [];
		this.idButton = 1;

	}

	onNoClick(): void {
		this.dialogRef.close();
	}

	ngOnInit() {
		this.titleModal =  "Crear botón";
		this.msjFather = this.data;
		this.orderMsjDefault = 0
	}

	close() {
		this.dialogRef.close();
	}

	openMsjExistent({order}:Message){

		const dialogConfig = new MatDialogConfig();

		dialogConfig.disableClose = true;
		dialogConfig.autoFocus = false;

		dialogConfig.data = {order}

		const dialogRef = this.dialog.open(ElegiMsjComponent, dialogConfig);

		dialogRef.afterClosed().subscribe(
			(objMsj) => {
				if (objMsj != null) {            	
					this.newButtonMsj()
					this.buttonForm.patchValue({
						buttonMsj : objMsj.subtitle
					})
					this.titleForm = 'Vincular con el mensaje :' +objMsj.title;
				}
		});
	}

	newButtonMsj(){

		this.dialogRef.close();
		let objButton = {
			status: true,
			title: this.buttonForm.controls.title.value
		}
		console.log(this.msjFather)

		this.formButtonService.setNewMsjButton(objButton, this.msjFather)
	}

}
