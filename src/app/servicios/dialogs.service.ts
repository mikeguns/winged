import { Injectable } from '@angular/core';
import Swal from 'sweetalert2'

@Injectable({
  providedIn: 'root'
})
export class DialogsService {

  constructor() { }

  success(mensaje){
    Swal(mensaje)
  }

  error(mensaje){
    Swal(mensaje)
  }
}
