import { AssociatepageService } from './../associatepage/associatepage.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http'
import { CampaniaDataService } from './campania-data.service'
import { MensajesService } from './mensajes.service'
import { DialogsService } from './dialogs.service'
import { AppConstant } from '../app.constant';

interface UsuarioDATA {
   description:string,
   pageId:string,
   token:string,
   url:string
}

@Injectable({
  providedIn: 'root'
})
export class EnvioDataService {
   private pagesAssociated : UsuarioDATA[]

   
   urlBroadcast: string;


   private headers = new HttpHeaders({
     'Content-Type': 'application/json; charset=utf-8',
     'Access-Control-Allow-Origin':'*',
     'Access-Control-Allow-Methods': 'POST'
   })

   constructor(
     private http : HttpClient,
     private mensaje : MensajesService,
     private dialogs : DialogsService,
     private campaniaData : CampaniaDataService,
     private associatePage: AssociatepageService,
     private appConstant: AppConstant
   ) {
       let constants = this.appConstant.getConstantApp();
       this.urlBroadcast = `${constants.url}api/broadcast`;
    }

    dataAEnviar() {
      let estructura
      let datosDeUsuarioRAW = sessionStorage.getItem('pagesAssocieted')
      let datosDeUsuario = JSON.parse(datosDeUsuarioRAW)

      estructura = {
           pageAccessToken: datosDeUsuario.token,
           pageId: datosDeUsuario.pageId,
           type: "message",
           startingStep:
           {
              stepContent:
              [
                 {
                   attachment:
                   {
                      type: "template",
                      payload:
                      {
                         template_type: "generic",
                         elements:
                         [
                            {
                              title: this.campaniaData.mensajesData[0].title,
                              image_url: "",
                              subtitle: " ",
                              buttons: []
                           }
                        ]
                     }
                  }
               }
            ],
            order: 0
         },
         bodySteps: []
      }

      this.campaniaData.mensajesData.forEach((msj) => {
       if(msj.order > 0 && msj.padre != -1) {

          if(msj.activo) {
             let botones:any[] = []

             msj.buttons.forEach(boton => {
                botones.push({
                  type: "postback",
                  //payload: `#type#step#type##order#${boton.vinculado}#order#`,
                    payload: `||type||step||type||||order||${boton.vinculado}||order||`,
                  title: boton.title
                })
             })

             estructura.bodySteps.push({
               stepContent:
               [{
                attachment:
                {
                  type: "template",
                  payload:
                  {
                    template_type: "generic",
                    elements:
                    [
                      {
                        title: msj.title,
                        image_url: "",
                        subtitle: " ",
                        buttons: botones
                      }
                    ]
                  }
                }
               }],

               order: msj.order,
            })
         }

       }

       if (msj.order == 0) {
          let botones:any[] = []

          msj.buttons.forEach(boton => {
             botones.push({
               type: "postback",
                 payload: `||type||step||type||||order||${boton.vinculado}||order||`,
               title: boton.title
             })
          })

          estructura.startingStep.stepContent[0].attachment.payload.elements[0].buttons = botones

       }
     })

      console.table(estructura.bodySteps)
      console.log(estructura)

      return estructura
   }

   enviarData() {


      this.http.post(
         this.urlBroadcast,
         this.dataAEnviar(),
         { headers: this.headers }
      ).subscribe(
       (data) => {
         console.log(data);
         this.dialogs.success('¡Campaña enviada satisfactoriamente!')
      },
      (error:HttpErrorResponse) => {
         console.log(error)
         this.dialogs.error('Error al enviar la campaña :(')
      }
     )
   }

}
