import { Injectable } from '@angular/core';
import { BotonesService, BotonDATA } from './botones.service';
import { CampaniaDataService } from './campania-data.service';
import Swal from 'sweetalert2';

export interface MensajeDATA {
   nombre:string,
   title:string,
   order:number,
   padre?:number,
   hijos:number,
   seMuestra?:boolean,
   activo:boolean,
   buttons:BotonDATA[],
}

@Injectable()

export class MensajesService {

   // PROPIEDADES

   constructor(
      private boton : BotonesService,
      private campaniaData : CampaniaDataService,
   ) {}


   // MÉTODOS PÚBLICOS

   public crear(padre:number) {

      if(padre >= 0) {
         if(this.campaniaData.mensajesData[padre].hijos < 3) {
            this.campaniaData.mensajesData.push(this.estructuraDeMensaje(padre))
            this.campaniaData.mensajesData[padre].hijos++
            this.boton.crearVinculadoAHijo(padre)
         }
      }

      else {
         this.campaniaData.mensajesData.push(this.estructuraDeMensaje(padre))
      }

   }


   public borrar(order:number) {
      this.campaniaData.mensajesData[order].activo = false
      this.desasignar(order)

      this.actualizarPadreDeMsjBorrado(order)

      this.campaniaData.mensajeFueBorrado = true
   }


   private actualizarPadreDeMsjBorrado(order:number) {
      if(this.campaniaData.mensajesData[this.campaniaData.mensajesData[order].padre]) {
         let padre:MensajeDATA = this.campaniaData.mensajesData[this.campaniaData.mensajesData[order].padre];
         padre.hijos--

         for(let i = 0; i < padre.buttons.length; i++) {
            if(padre.buttons[i].vinculado == order) {
               this.boton.borrar(padre.order, i);
               break;
            }
         }

         this.campaniaData.order = padre.order

      }else{
         this.campaniaData.order = 0
      }
   }


   public permitirAgregarMensajeHijo(padre) : boolean {
      let mensaje:MensajeDATA = this.campaniaData.mensajesData[padre]

      if(mensaje.buttons.length < 3) {
         return true
      }

      else if(mensaje.buttons.length == 3) {
         if(mensaje.hijos < mensaje.buttons.length) {
            console.log("ES MENOR")
            for(let boton of mensaje.buttons) {
               if(boton.vinculado) {
                  return false
               }

               else {
                  return true
               }
            }
         }
      }
   }


   public desasignar(padre:number) {
      this.campaniaData.mensajesData.forEach(hijo => {
         if(hijo.padre == padre) {
            hijo.padre = -1

            if (hijo.nombre == `Hijo sin nombre #${hijo.order}`) {
               hijo.nombre = `Mensaje no asignado #${hijo.order}`
            }
         }
      })
   }

   // MÉTODOS PRIVADOS
   private estructuraDeMensaje(padre) : MensajeDATA {
      let base : MensajeDATA = {
         title: '',
         nombre: `Hijo sin nombre #${this.campaniaData.mensajesData.length}`,
         order: this.campaniaData.mensajesData.length,
         padre: padre,
         hijos:0,
         buttons:[],
         activo:true,
         seMuestra:true,
      }

      if(padre == -1) base.nombre = `Mensaje no asignado #${this.campaniaData.mensajesData.length}`

      return base
   }
}
