import { Injectable } from '@angular/core';
import { BotonDATA } from './botones.service';
import { MensajeDATA } from './mensajes.service';

@Injectable({
  providedIn: 'root'
})
export class CampaniaDataService {

   public order:number = 0;
   public mensajeFueBorrado:boolean = false;

   public mensajesData:MensajeDATA[] = [{
      title:'',
      nombre:"Mensaje inicial",
      order: 0,
      buttons:[],
      activo:true,
      seMuestra:true,
      padre:null,
      hijos:0,
   }]

   constructor() { }

   public cargarMensajes(padre:number) : MensajeDATA[] {
      let hijos : MensajeDATA[] = []

      for(let mensaje of this.mensajesData) {
         if(mensaje.padre == padre) {
            if(mensaje.activo) {
               hijos.push(mensaje)
               if(hijos.length == 3) {
                  break;
               }

               // if(hijos.length == this.mensajesData[padre].hijos) {
               //    break;
               // }
            }
         }
      }

      return hijos
   }

}
