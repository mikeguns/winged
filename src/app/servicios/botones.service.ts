import { Injectable } from '@angular/core';
import { Payload } from './../campania/form-msj/form-msj';
import { MensajeDATA } from './mensajes.service';
import { CampaniaDataService } from './campania-data.service';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2'

export interface BotonDATA {
  title:string,
  vinculado?:number
}

@Injectable()

export class BotonesService {

   // PROPIEDADES GLOBALES
   private nuevoBotonTitulo:string;

   public enEdicion:any = { title: '', order: null, index: null, mensaje: null, vinculado: null }

   public mensajeAVincular:MensajeDATA;
   public mensajeAVincularPlaceholder:MensajeDATA;
   private vinculadoEsExistente:boolean;
   private vinculadoEsNuevo:boolean;

   constructor(
      private campaniaData? : CampaniaDataService,
      private modal? : NgbModal,
      private modalActivo? : NgbActiveModal,
   ) { }


   // MÉTODOS PÚBLICO

   /** Crea un boton nuevo.
   Arg: "order" debe ser la propiedad "order" del mensaje que contendrá el botón.
   Arg: "vínculo" (opcional) debe ser la propiedad "order" del mensaje al que está vinculado el botón. */
   public crear(order:number, vinculo?:number) {
      if(this.campaniaData.mensajesData[order].buttons.length < 3) {
         let botonData:BotonDATA = { title: this.nuevoBotonTitulo, vinculado: vinculo }
         this.campaniaData.mensajesData[order].buttons.push(botonData);

         console.table(botonData)
      }

      else {
         console.log("Ya existen tres botones")
      }
   }


   public crearSegunVinculado () {
      if(this.vinculadoEsExistente && !this.vinculadoEsNuevo) {
         this.crearVinculadoAExistente()
      }

      else if(this.vinculadoEsNuevo && !this.vinculadoEsExistente) {
         this.crearVinculadoANuevo()
      }

      else {
         this.crear(this.campaniaData.order)
      }

   }


   public crearVinculadoAHijo(order:number) {
      this.nuevoBotonTitulo = `Paso #${this.campaniaData.mensajesData.length - 1}`
      this.crear(order, this.campaniaData.mensajesData.length - 1)
   }


   private crearVinculadoANuevo() {
      if(this.campaniaData.mensajesData[this.campaniaData.order].hijos < 3) {

         this.campaniaData.mensajesData.push(this.mensajeAVincular)
         this.crear(this.campaniaData.order, this.mensajeAVincular.order);
         this.campaniaData.mensajesData[this.campaniaData.order].hijos++

         this.actualizarMensajePadre(this.campaniaData.order)
      }

      this.resetMensajeAVincular()
   }


   actualizarMensajePadre(order) {
      if(this.campaniaData.mensajesData[order].padre != null) {
         let padre = this.campaniaData.mensajesData[this.campaniaData.order].padre
         console.log(this.campaniaData.mensajesData[padre])

         if(padre >= 0) {
            if(this.campaniaData.mensajesData[padre].hijos < 3) {
               this.campaniaData.mensajesData[padre].hijos++
               console.log(this.campaniaData.mensajesData[padre])
            }
         }
      }
   }

   // FUNCION PLACEHOLDER, SE DEBE AJUSTAR LUEGO
   private actualizarMensajePadreAlBorrarHijo() {
      let padre = this.campaniaData.mensajesData[this.enEdicion.order]
      padre.hijos--
      console.log(padre)
   }

   private crearVinculadoAExistente() {

      this.crear(this.campaniaData.order, this.mensajeAVincular.order);
      this.campaniaData.mensajesData[this.mensajeAVincular.order].padre = this.campaniaData.order;

      if(this.campaniaData.mensajesData[this.mensajeAVincular.order].nombre == `Mensaje no asignado #${this.mensajeAVincular.order}`) {
         this.campaniaData.mensajesData[this.mensajeAVincular.order].nombre = `Hijo sin nombre #${this.mensajeAVincular.order}`
      }

      this.resetMensajeAVincular()
   }


   // MÉTODOS PARA ELIMINAR BOTONES

   /** Elimina un botón de un mensaje.
   Arg: "order" debe ser la propiedad "order" del mensaje que contendrá el botón. */
   public borrar(order:number, index:number) {
      if(this.enEdicion.order != null) {
         this.actualizarMensajePadreAlBorrarHijo()
         this.desasignarMensaje()
      }
      this.campaniaData.mensajesData[order].buttons.splice(index, 1);
   }


   // MÉTODOS PARA VINCULAR BOTONES

   private vincularMensajeExistente() {
      this.vinculadoEsExistente = true;
      this.vinculadoEsNuevo = false;

      this.mensajeAVincular = this.mensajeAVincularPlaceholder

      console.log(this.vinculadoEsExistente, this.vinculadoEsNuevo)
      console.table(this.mensajeAVincularPlaceholder)
      console.table(this.mensajeAVincular)
   }


   private vincularMensajeNuevo() {
      this.vinculadoEsExistente = false;
      this.vinculadoEsNuevo = true;

      this.mensajeAVincular = {
         title: '',
         nombre: `Hijo sin nombre #${this.campaniaData.mensajesData.length}`,
         order: this.campaniaData.mensajesData.length,
         padre: this.campaniaData.order,
         hijos:0,
         buttons:[],
         activo:true,
         seMuestra:true,
      }

      this.enEdicion.mensaje = this.mensajeAVincular
   }


   private resetMensajeAVincular() {
      this.mensajeAVincular = null;
      this.mensajeAVincularPlaceholder = null
      this.vinculadoEsExistente = false;
      this.vinculadoEsNuevo = false;
   }

   private resetEnEdicionInfo() {
      this.enEdicion = { title: '', order: null, index: null, mensaje: null, vinculado: null }
   }


   // MÉTODO PARA EDITAR BOTON

   public editar() {
      this.campaniaData.mensajesData[this.enEdicion.order].buttons[this.enEdicion.index].title = this.enEdicion.title;

      if(this.mensajeAVincular != null) {
         this.campaniaData.mensajesData[this.enEdicion.order].buttons[this.enEdicion.index].vinculado = this.mensajeAVincular.order
      }

      else {
         this.desasignarMensaje();
         this.campaniaData.mensajesData[this.enEdicion.order].buttons[this.enEdicion.index].vinculado = null;
      }

      this.resetEnEdicionInfo()

      console.table(this.campaniaData.mensajesData[this.enEdicion.order].buttons[this.enEdicion.index])
   }

   public desasignarMensaje() {
      let mensajeADesasignar = this.campaniaData.mensajesData[this.enEdicion.vinculado]
      console.log(this.campaniaData.mensajesData[this.enEdicion.vinculado])
      mensajeADesasignar.padre = -1

      if (mensajeADesasignar.nombre == `Hijo sin nombre #${mensajeADesasignar.order}`) {
         mensajeADesasignar.nombre = `Mensaje no asignado #${mensajeADesasignar.order}`
      }

   }


   public editarSegunVinculado() {
      if(this.vinculadoEsExistente && !this.vinculadoEsNuevo) {
         this.editarVinculadoAExistente()
      }

      else if(this.vinculadoEsNuevo && !this.vinculadoEsExistente) {
         this.editarVinculadoANuevo()
      }

      else {
         this.editar()
      }

   }


   private editarVinculadoANuevo() {
      if(this.campaniaData.mensajesData[this.campaniaData.order].hijos < 3) {

         this.campaniaData.mensajesData.push(this.mensajeAVincular)
         this.editar();
         this.campaniaData.mensajesData[this.campaniaData.order].hijos++

         this.actualizarMensajePadre(this.campaniaData.order)
      }

      this.resetMensajeAVincular()
   }


   private editarVinculadoAExistente() {
      this.campaniaData.mensajesData[this.mensajeAVincular.order].padre = this.campaniaData.order;

      if(this.campaniaData.mensajesData[this.mensajeAVincular.order].nombre == `Mensaje no asignado #${this.mensajeAVincular.order}`) {
         this.campaniaData.mensajesData[this.mensajeAVincular.order].nombre = `Hijo sin nombre #${this.mensajeAVincular.order}`
      }

      this.editar();
      this.resetMensajeAVincular()
   }


   public recogerDatos(order:number, index:number, vinculado:number) {
      this.enEdicion.order = order;
      this.enEdicion.index = index;
      this.enEdicion.mensaje = this.campaniaData.mensajesData[order];
      this.enEdicion.vinculado = vinculado;
      this.enEdicion.title = this.enEdicion.mensaje.buttons[index].title;

      if(vinculado){
         this.mensajeAVincular = this.campaniaData.mensajesData[vinculado]
         this.vinculadoEsExistente = true;
      }

      else {
         this.resetMensajeAVincular()
      }

      console.log(`vinculado: ${vinculado}`)
      console.table(this.enEdicion)
      console.table(this.mensajeAVincular)

   }

}
