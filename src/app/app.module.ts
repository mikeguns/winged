import { MensajesService } from './servicios/mensajes.service';
import { EnvioDataService } from './servicios/envio-data.service';
import { DialogsService } from './servicios/dialogs.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { CampaniaComponent } from './campania/campania.component';
import { CrecimientoComponent } from './crecimiento/crecimiento.component';
import { SuscriptoresComponent } from './suscriptores/suscriptores.component';
import { ConfiguracionComponent } from './configuracion/configuracion.component';
import { LoginComponent } from './core/login/login.component';
import { CoreComponent } from './core/core.component';
import { MenuComponent } from './core/menu/menu.component';
import { HttpClientModule } from '@angular/common/http';
import { Ng2TableModule } from 'ng2-table/ng2-table';
import { AlertModule } from 'ngx-bootstrap';
import { Angular2FontawesomeModule } from 'angular2-fontawesome/angular2-fontawesome';
import { AppRoutingModule } from './/app-routing.module';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule } from "@angular/forms";
import { InterceptorModule } from './interceptor.module';
import { FacebookModule } from 'ngx-facebook';
import { AppConstant } from './app.constant';
import { HeaderComponent } from './core/header/header.component';
import { LoginService } from './core/login/login.service';
import { AssociatepageService } from './associatepage/associatepage.service';
import { AssociatepageComponent } from './associatepage/associatepage.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CrecimientoDialog } from './crecimiento/crecimiento.component';
import { MatCardModule, MatExpansionModule, MatIconModule, MatButtonModule, MatInputModule, MatDialogModule, MatTabsModule, MatTreeModule } from '@angular/material';

import { FormMsjComponent } from './campania/form-msj/form-msj.component';
import { MenuLeftComponent } from './campania/menu-left/menu-left.component';
import { FormButtonComponent } from './campania/form-button/form-button.component';
import { ElegiMsjComponent } from './campania/elegi-msj/elegi-msj.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormMsjService } from './campania/form-msj/form-msj.service';
import { DeleteMsjComponent } from './campania/delete-msj/delete-msj.component';
import { NestableModule } from 'ngx-nestable'
import { NgxTreeDndModule } from 'ngx-tree-dnd'; // here
import { LocationStrategy, HashLocationStrategy } from "@angular/common";
import { BotonesService } from './servicios/botones.service';
import { MensajesComponent } from './campania/mensajes/mensajes.component';
import { BotonesComponent } from './campania/botones/botones.component';
import { PreviewComponent } from './campania/preview/preview.component'


@NgModule({
  declarations: [
    AppComponent,
    CampaniaComponent,
    CrecimientoComponent,
    SuscriptoresComponent,
    ConfiguracionComponent,
    CoreComponent,
    MenuComponent,
    LoginComponent,
    HeaderComponent,
    AssociatepageComponent,
    CrecimientoDialog,
    FormMsjComponent,
    MenuLeftComponent,
    FormButtonComponent,
    ElegiMsjComponent,
    DeleteMsjComponent,
    BotonesComponent,
    PreviewComponent,
    MensajesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    InterceptorModule,
    Ng2TableModule,
    NgbModule,
    NgbModule.forRoot(),
    PaginationModule.forRoot(),
    ModalModule.forRoot(),
    FacebookModule.forRoot(),

    Angular2FontawesomeModule,
    AppRoutingModule,
    FormsModule,
    MatCardModule,
    MatExpansionModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatTabsModule,
    MatTreeModule,
    MatInputModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    NestableModule,
    NgxTreeDndModule,
  ],
  providers: [
    AppConstant,
    LoginService,NgbActiveModal,
    AssociatepageService,
    FormMsjService,
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    BotonesService,
    MensajesService,
    EnvioDataService,
    DialogsService

  ],
  bootstrap: [AppComponent],
  entryComponents: [CrecimientoDialog, FormButtonComponent, ElegiMsjComponent, DeleteMsjComponent],
})
export class AppModule { }
