import { Component, OnInit } from '@angular/core';
import { AppModule } from '../app.module';
import { ConfiguracionService } from './configuracion.service';
import { AssociatepageService } from '../associatepage/associatepage.service';
import { FormGroup, FormControl } from '@angular/forms';
import { AppConstant } from '../app.constant';

@Component({
  selector: 'app-configuracion',
  templateUrl: './configuracion.component.html',
  styleUrls: ['./configuracion.component.sass']
})
export class ConfiguracionComponent implements OnInit {
	step = 0;
	subscribeForm: FormGroup;
	
	setStep(index: number) {
		this.step = index;
	}

	nextStep() {
		this.step++;
	}

	prevStep() {
		this.step--;
	}
    delimiter: string;

    constructor(private configuracionService: ConfiguracionService,
        private appConstant: AppConstant) {
        let confMessages = this.appConstant.getConfigurationMessages();
        this.delimiter = confMessages.characterDelimiter;
    }

	ngOnInit() {
		this.subscribeForm = new FormGroup({
	    	titulo: new FormControl(''),
	    	subtitulo: new FormControl(''),
	    	imgURL: new FormControl(''),
	    	boton1: new FormControl(''),
	    	boton2: new FormControl(''),
	  	});
	  	this.showInitMessageData();
	}

  	showInitMessageData() {
	  	this.configuracionService.getInitMessageData().subscribe((data : any) => {
	  		let configurationList : Array<any> = data.configurationList;
	  		if(null !== data && null !== configurationList[0].key){
	  			for (var i = 0; i < configurationList.length; i++) {
	  				let elements = configurationList[i].value.attachment.payload.elements;
	  				for (var j = 0; j < elements.length; j++) {
	  					let element = elements[j];
		   				this.subscribeForm.patchValue({
						    titulo: element.title,
						    subtitulo: element.subtitle,
						    imgURL: element.image_url,
						    boton1: element.buttons[0].title,
						    boton2: element.buttons[1].title
						});
	  				}		
	  			}
			}
	    });
	}

	saveInitMessageData(){
		let objForm : any =
		{
		  "attachment": {
		    "type": "template",
		    "payload": {
		      "template_type": "generic",
		      "elements": [
		        {
		          "title": this.subscribeForm.value.titulo,
		          "image_url": this.subscribeForm.value.imgURL,
		          "subtitle": this.subscribeForm.value.subtitulo,
		          "buttons": [
		            {
		              "type": "postback",
                      "payload": this.delimiter + "type" + this.delimiter + "addsubscriptor" + this.delimiter + "type" + this.delimiter,
		              "title": this.subscribeForm.value.boton1
		            },
		            {
		              "type": "postback",
		              "payload": this.delimiter + "type" + this.delimiter + "deletesubscriptor" + this.delimiter + "type" + this.delimiter,
		              "title": this.subscribeForm.value.boton2
		            }
		          ]
		        }
		      ]
		    }
		  }
		};

		this.configuracionService.setInitMessageData(objForm).subscribe(
			res => {
	        },
	        err => {
	        }
	    );
	}
}
