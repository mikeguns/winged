import { Injectable, Input, Output, EventEmitter } from '@angular/core';
import { AppConstant } from '../app.constant';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { Form } from '@angular/forms';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { AssociatepageService } from '../associatepage/associatepage.service';
import { map } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class ConfiguracionService {
	appCons: any;
    pageSelected: any;
    urlBase: any;
	url: any;

	constructor(private appConstant: AppConstant,
		private http: HttpClient,
		private associatePage: AssociatepageService) {
		this.appCons = this.appConstant.getConstantApp();
        this.pageSelected = associatePage.getPagesAssocietedStorage();
        this.urlBase = this.appCons.url;
        this.url = this.urlBase + 'api/configuration?pageId=';

	}

	getInitMessageData() {
		return this.http.get(this.url + this.pageSelected.pageId + '&key=init_message');
	}

	setInitMessageData(form: any) {
		let headers: any = new Headers({ 'Content-Type': 'application/json' });
		let options: any = new RequestOptions({ headers: headers });
		return this.http.post(this.url + this.pageSelected.pageId + '&key=init_message', form, options);
	}
}
